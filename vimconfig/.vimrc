set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
set noshowmode

set term=xterm-256color

map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Map ; to : for vim commands in normal mode
nmap ; :

execute pathogen#infect()
execute pathogen#helptags()
filetype plugin on
set nocompatible  " We don't want vi compatibility.

let mapleader=","
" LEADER MAPPINGS
nmap <Leader>ev :e ~/.vimrc<CR>
nmap <Leader>sv :o ~/.vimrc<CR>
map <Leader>s :call RunCurrentSpecFile()<CR>
autocmd BufRead,BufNewFile *.html.erb setlocal filetype=eruby

" Add recently accessed projects menu (project plugin)
set viminfo^=!

" Minibuffer Explorer Settings
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

" alt+n or alt+p to navigate between entries in QuickFix
map <silent> <m-p> :cp <cr>
map <silent> <m-n> :cn <cr>

map <silent> <F2> :Tlist <CR>

map  <C-t> :tabedit<CR>
map  <C-l> :tabn<CR>
map  <C-h> :tabp<CR>
map  <C-j> :bnext<CR>
map  <C-k> :bprevious<CR>
" NERDTree Functions
" Ctrl-n to toggle NERDTree
map <silent> <C-n> :NERDTreeToggle <CR>

" Change which file opens after executing :Rails command
let g:rails_default_file='config/database.yml'

syntax enable

set cf  " Enable error files &amp; error jumping.
set clipboard+=unnamed  " Yanks go on clipboard instead.
set history=256  " Number of things to remember in history.
set autowrite  " Writes on make/shell commands
set ruler  " Ruler on
set nu  " Line numbers on
set nowrap  " Line wrapping off
set timeoutlen=250  " Time to wait after ESC (default causes an annoying delay)
" colorscheme vividchalk  " Uncomment this to set a default theme

" Formatting (some of these are for coding in C and C++)
set ts=2  " Tabs are 2 spaces
set bs=2  " Backspace over everything in insert mode
set shiftwidth=2  " Tabs under smart indent
set nocp incsearch
set cinoptions=:0,p0,t0
set cinwords=if,else,while,do,for,switch,case
set formatoptions=tcqr
set cindent
set autoindent
set smarttab
set expandtab

" Visual
set showmatch  " Show matching brackets.
set mat=5  " Bracket blinking.
set list
" Show $ at end of line and trailing space as ~
set lcs=tab:\ \ ,eol:$,trail:~,extends:>,precedes:<
set novisualbell  " No blinking .
set noerrorbells  " No noise.
set laststatus=2  " Always show status line.

" gvim specific
set mousehide  " Hide mouse after chars typed
set mouse=a  " Mouse in all modes


